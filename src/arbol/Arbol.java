package arbol;

/**
 *
 * @author Tita
 */
public class Arbol {

    byte[][] matriz;

    public byte[][] matriz(String pre, String ino) {
        char[] preo = pre.toCharArray();
        char[] inor = ino.toCharArray();
        matriz = new byte[pre.length()][inor.length];
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                if (preo[i] == inor[j] && matriz[i][j] != 1) {
                    matriz[i][j] = 1;
                    rellenarUnos(i, j);
                }
            }
        }
        return matriz;
    }

    private void rellenarUnos(int columna, int fila) {
        for (int i = columna; i < matriz.length; i++) {
            matriz[i][fila] = 1;
        }
    }

    
    public String toStringMatriz() {
        String rta = "";
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                rta += "\t" + matriz[i][j];
            }
            rta += "\n";
        }
        return rta;
    }

}
