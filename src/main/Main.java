   
package main;

import util.*;
import arbol.Arbol;
import java.util.Iterator;
import static javafx.scene.input.KeyCode.T;

/**
 *
 * @author Tita
 */
public class Main {

        static ArbolBinario tree = new ArbolBinario();

    public static void llenar(ArbolBinario a) {
         a.insertarHijoDer("", "x");
        a.insertarHijoDer("x", "j");
        a.insertarHijoDer("j", "k");
        a.insertarHijoDer("k", "m");
        a.insertarHijoIzq("k", "l");
        a.insertarHijoDer("m", "ñ");
        a.insertarHijoIzq("m", "u");
        a.insertarHijoIzq("l", "w");
        a.insertarHijoDer("w", "q");
        a.insertarHijoIzq("q", "z");
        a.insertarHijoIzq("ñ", "o");
        a.insertarHijoIzq("o", "p");
        a.insertarHijoIzq("x", "a");
        a.insertarHijoIzq("a", "b");
        a.insertarHijoDer("b", "c");
        a.insertarHijoIzq("b", "e");
        a.insertarHijoDer("c", "d");
        a.insertarHijoIzq("e", "i");
        a.insertarHijoDer("e", "f");
        a.insertarHijoDer("i", "n");
        a.insertarHijoDer("n", "r");
        a.insertarHijoIzq("d", "g");
        a.insertarHijoDer("g", "h");

    }

    public static void main(String[] args) throws Exception {

        ArbolBinarioBusqueda<Integer> a = new ArbolBinarioBusqueda();
        a.insertar(9);
        a.insertar(7);
        a.insertar(11);
        a.insertar(5);
        a.insertar(3); 
        a.insertar(6);
        a.insertar(4);        
        a.insertar(1);
        a.insertar(8);
        a.insertar(10);
        a.insertar(12);
        a.insertar(13);        
        a.insertar(14);
        
        ArbolBinario<String> b = new ArbolBinario();

        b.insertarHijoDer("", "50");
        b.insertarHijoDer("50", "60");
        b.insertarHijoIzq("50", "40");
        b.insertarHijoDer("40", "45");
        b.insertarHijoIzq("40", "30");
        b.insertarHijoIzq("60", "57");
        b.taxonomia();
        System.out.println(b.taxonomia() );

    

//        llenar(tree);
//        Pila<String>p= tree.caminoRaiz2("p");
//        System.out.println("camino: "+p.toString());
       // System.out.println(tree.getAltura());
// 
    }       
 
}